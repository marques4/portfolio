import React, {useEffect, useRef} from "react";
import {B, color, G, R} from "./utils/AnimatedBackgroundFunctions";

interface AnimatedBackgroundProps {
  canvasWidth: string;
  canvasHeight: string;
  canvasAnimationSpeed: number;
}

const AnimatedBackground: React.FunctionComponent<AnimatedBackgroundProps> = ({canvasWidth, canvasHeight, canvasAnimationSpeed}) => {
  const canvasRef = useRef<HTMLCanvasElement>(null)
  useEffect(() => {
    const canvas = canvasRef.current;

    if (canvas) {
      const ctx = canvas.getContext("2d");

      let time = 0;

      const loop = function () {
        for (let x = 0; x <= 42; x++) {
          for (let y = 0; y <= 42; y++) {
            color(ctx, {
              x,
              y,
              r: R(x, y, time),
              g: G(x, y, time),
              b: B(x, y, time),
            });
          }
        }

        time = time + canvasAnimationSpeed;

        window.requestAnimationFrame(loop);
      };

      loop();
    }
  }, []);



  return <canvas style={{width: '100%', height: '100%'}} ref={canvasRef} width={'32px'} height={'32px'}/>;
}

export default AnimatedBackground;

//@ts-ignore
//TODO: FIX LATER
const C1 = 91;
const C2 = 64;
//@ts-ignore
export const color = function (context:  CanvasRenderingContext2D | null, { x, y, r, g, b }) {
  if(context) {
    context.fillStyle = `rgb(${r}, ${g}, ${b})`;
    context.fillRect(x, y, 1, 1);
  }
};
//@ts-ignore
export const R = function (x, y, time) {
  return Math.floor(C1 + C2 * Math.cos((x * x - y * y) / 300 + time));
};
//@ts-ignore
export const G = function (x, y, time) {
  return Math.floor(
    C1 +
    C2 *
    Math.sin(
      (x * x * Math.cos(time / 4) + y * y * Math.sin(time / 3)) / 300
    )
  );
};
//@ts-ignore
export const B = function (x, y, time) {
  return Math.floor(
    C1 +
    C2 *
    Math.sin(
      5 * Math.sin(time / 9) +
      ((x - 100) * (x - 100) + (y - 100) * (y - 100)) / 1100
    )
  );
};

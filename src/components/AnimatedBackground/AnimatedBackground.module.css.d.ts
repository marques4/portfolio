declare const styles: {
  readonly 'footer': string;
  readonly 'background-container': string;
  readonly 'page-content': string;
  readonly 'display': string;
  readonly 'content' : string;
  readonly 'content-card' : string;
  readonly 'content-card-info' : string;
  readonly 'hannah': string;
  readonly 'container': string;
  readonly 'background-cover': string;
}

export = styles;

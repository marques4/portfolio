import React from "react";
import AnimatedBackground from "./AnimatedBackground/AnimatedBackground";
import styles from './AnimatedBackground/AnimatedBackground.module.css';
import logo from '../loading.gif';
import file from '../public/Resume.pdf';
function App() {
  return (
    <>
      <div className={styles.display}>
        <div className={styles.container}>
        </div>
      </div>
      <div className={styles["page-content"]}>
        <div >
          <img src={logo} alt={"loading-gif"}/>
          <p style={{textAlign: 'center', color: 'white'}} className={styles.hannah}>❤ Hannah - <strong>02/12/22</strong></p>
        </div>
        <div className={styles.content}>
          <div className={styles["content-card"]}>
            <div className={styles["content-card-info"]}>
              <h1><span style={{fontWeight: '600'}}>Software Engineer</span><span style={{opacity: '0.75'}}> @ Ocado Technology</span></h1>
              <p>Jun 2022 - Current // Hatfield UK.</p>
            </div>
          </div>
          <div className={styles["content-card"]}>
            <div className={styles["content-card-info"]}>
              <h1><span style={{fontWeight: '600'}}>Games Technology</span><span style={{opacity: '0.75'}}> @ University of Suffolk</span></h1>
              <p>Sept 2018 - Sept 2021 // Ipswich UK.</p>
            </div>
          </div>
        </div>
        <h1><a href={file}>Resume</a></h1>
      </div>
      <div className={styles["background-cover"]}></div>
      <div className={styles["background-container"]}>
        <AnimatedBackground canvasWidth={'150px'} canvasHeight={'150px'} canvasAnimationSpeed={0.05}/>
      </div>
      <div className={styles.footer}>
        &copy; Marques 2023
      </div>

    </>
  )
}

export default App
